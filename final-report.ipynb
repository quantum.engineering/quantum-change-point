{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Quantum Change Point {-}\n",
    "Final report for the postgraduate course on Quantum Engineering at the Universitat Politènica de Catalunya (UPC) school. <br>\n",
    "Written by Isidoro Legido and Victor Gaspar, and supervised by Michalis Skotiniotis. <br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Content {-}\n",
    "\n",
    "[1. Introduction](#introduction) <br>\n",
    "\n",
    "&ensp; [1.1. Problem description](#problem-description) <br>\n",
    "\n",
    "&ensp; [1.2. Approach](#approach) <br>\n",
    "\n",
    "[2. Work done](#work-done) <br>\n",
    "\n",
    "&ensp; [2.1. Fully bias measurement](#fully-bias-measurement) <br>\n",
    "\n",
    "&ensp; [2.2. Helstrom measurement](#helstrom-measurement) <br>\n",
    "\n",
    "&ensp; [2.3. Bayesian Helstrom measurement](#bayesian-helstrom-measurement) <br>\n",
    "\n",
    "&ensp; [2.4. Results comparison](#results-comparison) <br>\n",
    "\n",
    "[3. Conclusion](#conclusion) <br>\n",
    "\n",
    "[4. Future work](#future-work) <br>\n",
    "\n",
    "[5. References](#references) <br>\n",
    "\n",
    "[6. Supplemental material](#references) <br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\pagebreak$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Introduction <a class=\"anchor\" id=\"introduction\"></a>  {-}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1. Problem description <a class=\"anchor\" id=\"problem-description\"></a> {-}\n",
    "\n",
    "The change point problem is a well known problem in statistics aiming at **detecting a change** in a sequence of random variables. A classical literature scenario is that of two coins, where one coin is fair and the other is biased, e.g. the probability of getting heads is $2/3$ and the probability of obtaining tails is $1/3$. In this scenario one of the two coins is randomly selected and it is tossed continuously. After a random number of runs the coin is changed without previous notice. The task in this scenario is to detect as soon as possible that the coin was changed.<br>\n",
    "\n",
    "In our project we studied the quantum equivalent of this problem called 'quantum change point', mainly based on the ideas described in [[1]](#reference-1), [[2]](#reference-2), [[3]](#reference-3) and [[4]](#reference-4). <br>\n",
    "\n",
    "The quantum change point problem in our project is formulated as a problem of discriminating quantum states, for a sequence of $n=5$ particles emitted by a source, and the change happening in the k-th position. In this version of the problem, both initial and final states are known. <br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Problem description](images/source-detector.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Throughout this document, the following notation is going to be used to describe the different hypothesis.\n",
    "\n",
    "&emsp; $|H_k> = |\\psi>^{k-1} |\\phi>^{\\otimes n-k+1}$<br>\n",
    "\n",
    "where $k \\in [1,n]$ \n",
    "\n",
    "For the case of n=5 states and the change happpening on the $k_{th}$ position we have the following five possible hypothesis:  \n",
    "&emsp; $H_1 : |\\Phi> |\\Phi> |\\Phi> |\\Phi> |\\Phi>$  \n",
    "&emsp; $H_2 : |\\Psi> |\\Phi> |\\Phi> |\\Phi> |\\Phi>$  \n",
    "&emsp; $H_3 : |\\Psi> |\\Psi> |\\Phi> |\\Phi> |\\Phi>$  \n",
    "&emsp; $H_4 : |\\Psi> |\\Psi> |\\Psi> |\\Phi> |\\Phi>$  \n",
    "&emsp; $H_5 : |\\Psi> |\\Psi> |\\Psi> |\\Psi> |\\Phi>$  \n",
    "  \n",
    "Since the change can happen in any position, every hypothesis $H_k$ is equally likely, meaning they all have the same *a priori* probability $1/n$. <br>\n",
    "  \n",
    "Along the document we also use *s* (step) to denote the particle emited at a given moment. As we show in the table below, at each step the contribution of each hypothesis to the probability of finding $\\Phi$ or $\\Psi$ changes. For example, at step $s = 2$, only hypothesis $H_1$ and $H_2$ contribute to the probability of $\\Phi$  but at step $s = 3$ also hypothesis $H_3$ contributes to the probability of $\\Phi$.\n",
    "\n",
    "|Hypothesis |Step #1  |Step #2  |Step #3  |Step #4  |Step #5  |\n",
    ":----------:|:----:   |:----:   |:----:   |:----:   |:----:   |\n",
    "| $H_1$     |$\\Phi$   |$\\Phi$   |$\\Phi$   |$\\Phi$   |$\\Phi$   |\n",
    "| $H_2$     |$\\Psi$   |$\\Phi$   |$\\Phi$   |$\\Phi$   |$\\Phi$   |\n",
    "| $H_3$     |$\\Psi$   |$\\Psi$   |$\\Phi$   |$\\Phi$   |$\\Phi$   |\n",
    "| $H_4$     |$\\Psi$   |$\\Psi$   |$\\Psi$   |$\\Phi$   |$\\Phi$   |\n",
    "| $H_5$     |$\\Psi$   |$\\Psi$   |$\\Psi$   |$\\Psi$   |$\\Phi$   |\n",
    "  \n",
    "We denote the measurement element corresponding to our guess $|\\Phi>$ as $E_{\\Phi}$, and $E_{\\Psi}$ for our measurement over the quantum system $|\\Psi>$. Given $E_{\\Phi}, E_{\\Psi} \\geq 0$ and $E_{\\Phi} + E_{\\Psi} = 1$. <br>\n",
    "\n",
    "As shown in [[1]](#reference-1) the optimal detection of a change point requires a non-local measurement, i.e. a measurement performed over all the particles in the sequence. The maximum success probability is achieved by the Square Root Measurement: <br>\n",
    "\n",
    "&emsp; $P_s = \\frac{Tr\\sqrt(G)}{n}$  \n",
    "  \n",
    "with $G$ being the Gram matrix defined as $G_{xy} = <\\tilde{H_x}|\\tilde{H_y}>$\n",
    "\n",
    "The Square Root measurement consists of POVM elements ${E_y = {|e_y><e_y|}}$ with $|e_y> = \\rho^{-\\frac{1}{2}}|\\tilde{H_y}>$ where $|\\tilde{H_y}> = \\sqrt{p_y}|H_y>$ and $E_{\\rho} = \\sum_{x} p_x |H_{x}><H_{x}|$. <br>\n",
    "\n",
    "Since implementing the optimal measurement would require a quantum memory not yet available, it is also interesting to study  local strategies, i.e. a measurement performed as soon as the state arrives to the detector. Unlike non-local strategies, local measurements also provide the additional benefit of real-time results. <br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2. Approach <a class=\"anchor\" id=\"approach\"></a> {-}\n",
    "The aim of our project is to research different local measurement strategies and understand how close we can get to the optimal measurement with them. Additionally, these strategies were implemented using the IBM Q Experience. The strategies researched were:\n",
    "\n",
    "* Fully bias measurement\n",
    "> Each quantum system in the sequence is measured along the direction of one of the states.\n",
    "* Helstrom measurement\n",
    "> Each quantum state in the sequence is measured using a projective measurement in which the two measurement outcomes correspond to projectors onto the positive and negative eigenvalues, ataining the minimum-error probability, the Helstrom bound.\n",
    "* Bayesian Helstrom measurement\n",
    "> Each state in the sequence is measured using the Helstrom measurement, but after each measurement the measurement device is adjusted according to the past experimental result using Bayesian inference."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Work done <a class=\"anchor\" id=\"work-done\"></a> {-}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1. Fully bias measurement <a class=\"anchor\" id=\"fully-bias-measurement\"></a> {-}\n",
    "\n",
    "The simplest measurement strategy, called fully bias measurement, consist of measuring each particle, along the basis of one of the known states, immediatly after the particle is received, i.e. using the projective measurement:\n",
    "  \n",
    "&emsp; $E_{\\Phi} = |\\Phi><\\Phi|$  \n",
    "&emsp; $E_{\\Psi} = 1 - E_{\\Phi}$\n",
    "  \n",
    "Given this projective measurement, the conditional probabilities of successfully identifying one particle are:\n",
    "  \n",
    "&emsp; $P(E_{\\Phi}|\\Phi) = Tr(E_{\\Phi} |\\Phi><\\Phi|) = <\\Phi|E_{\\Phi}|\\Phi> = |<\\Phi|\\Phi>|^2 = 1$  \n",
    "&emsp; $P(E_{\\Psi}|\\Phi) = 1 - P(E_{\\Phi}|\\Phi) = 0$  \n",
    "&emsp; $P(E_{\\Phi}|\\Psi) = Tr(E_{\\Phi} |\\Psi><\\Psi|) = <\\Psi|E_{\\Phi}|\\Psi> = |<\\Psi|\\Phi>|^2 = c^2$  \n",
    "&emsp; $P(E_{\\Psi}|\\Psi) = 1 - P(E_{\\Phi}|\\Psi) = 1 - c^2$  \n",
    "  \n",
    "As it is clear from the first equation, our measurement apparatus can detect the state $\\Psi$ with 100% accuracy, detecting the change as soon as it happens.\n",
    "  \n",
    "In our case, the likelihood of an hypothesis given a measurement outcome, is the same as the conditional probabilities above. Therefore\n",
    "\n",
    "&emsp; $L(\\Phi|E_{\\Phi}) = 1$  \n",
    "&emsp; $L(\\Phi|E_{\\Psi}) = 0$  \n",
    "&emsp; $L(\\Psi|E_{\\Phi}) = c^2$  \n",
    "&emsp; $L(\\Psi|E_{\\Psi}) = 1 - c^2$  \n",
    "  \n",
    "Since $L(\\Phi|E_{\\Phi}) > L(\\Psi|E_{\\Phi})$ and $L(\\Psi|E_{\\Psi}) >= L(\\Phi|E_{\\Psi})$, our Maximum Likelihood Estimator (MLE) is then given by  \n",
    "  \n",
    "\n",
    "&emsp; $f(x)=\\left\\{\n",
    "    \\begin{array}{ll}\n",
    "        \\Phi  & \\mathrm{if\\ } E_{\\Phi}\\\\\n",
    "        \\Psi  & \\mathrm{if\\ } E_{\\Psi}\n",
    "    \\end{array}\n",
    "\\right.$\n",
    "  \n",
    "Since the states are independent and identically distributed, the probability of success of the 'Basic Global' measurement (i.e. not stopping when the state $\\Phi$ is measured) for a given experiment is determined by\n",
    "  \n",
    "&emsp; $P_{BG}= \\prod_{i=0}^{n_1}{P(E_{\\Phi}|\\Phi)}\\prod_{j=0}^{n_2}{P(E_{\\Psi}|\\Psi)} = 1^{n_1} \\times (1-c^2)^{n_2}$\n",
    "  \n",
    "where $n_1$ and $n_2$ is the number of $E_{\\Phi}$ and $E_{\\Psi}$ measurements, respectively.  \n",
    "  \n",
    "If we stop when the outcome $\\Psi$ is measured, the success probability of the 'Basic Local' measurement is improved, and it has the expression\n",
    "  \n",
    "&emsp; $P_{BL} = 1 - c^2 + \\frac{c^2}{n}$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2. Helstrom measurement <a class=\"anchor\" id=\"helstrom-measurement\"></a> {-}\n",
    "\n",
    "Helstrom measurement maximizes the probability of successfully distinguishing two non-orthogonal states by defining a projective measurement {$E_{\\Psi}$, $E_{\\Phi}$} that minimizes the probability of error.  \n",
    "  \n",
    "Notice that in this measurement strategy what we do is to apply the Helstrom measurement to all the particles in the sequence without updating the priors.\n",
    "  \n",
    "We can represent our two states $\\Psi$ and $\\Phi$ as:\n",
    "\n",
    "&emsp; $|\\Psi> = \\cos(\\theta)|0> + \\sin(\\theta)|1>$  \n",
    "&emsp; $|\\Phi> = \\cos(\\theta)|0> - \\sin(\\theta)|1>$  \n",
    "  \n",
    "with $\\theta \\in (0, \\pi/4)$. <br>\n",
    "\n",
    "Defining the probability of each state happening as:  \n",
    "\n",
    "&emsp; $P_{\\Psi}$ : probability of state $\\Psi$  \n",
    "&emsp; $P_{\\Phi}$ : probability of state $\\Phi$  \n",
    "  \n",
    "The probability of success of the Helstrom measurement is given by the following expression:  \n",
    "\n",
    "&emsp; $P_s = P_{\\Psi} Tr(E_{\\Psi}|\\Psi><\\Psi|) + P_{\\Phi} Tr(E_{\\Phi}|\\Phi><\\Phi|)$  \n",
    "  \n",
    "As shown in the supplemental material: [Probability of success for one state](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-ProbHelstrom1state.ipynb), this expression can be simplified using the properties of the trace as:  \n",
    "  \n",
    "&emsp; $P_s = P_{\\Psi} + Tr(E_{\\Psi} \\Gamma)$\n",
    "  \n",
    "where $\\Gamma = P_{\\Psi} |\\Psi><\\Psi| - P_{\\Phi} |\\Phi><\\Phi| = \n",
    "\\begin{pmatrix}\n",
    "(P_{\\Psi}-P_{\\Phi})cos^2(\\theta) & \\frac{1}{2}sin(2\\theta)\\\\\n",
    "\\frac{1}{2}sin(2\\theta)          & (P_{\\Psi}-P_{\\Phi})sin^2(\\theta)\n",
    "\\end{pmatrix}$ \n",
    "  \n",
    "Therefore, the optimal measurement is given by the projectors onto the eigenspaces of $\\Gamma$.\n",
    "\n",
    "As shown in the supplemental material: [Helstrom proof](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-ProofHelstrom.ipynb), $\\Gamma$ has two eigenvalues, one positive and another negative. The eigenvector associated with the positive value corresponds to the measurement of $\\Psi$ and the negative with $\\Phi$. The Helstrom measurement {$E_{\\Psi}$, $E_{\\Phi}$} can be defined as:  \n",
    "  \n",
    "- $E_{\\Psi} = |V_{+}><V_{+}|$  \n",
    "- $E_{\\Phi} = |V_{-}><V_{-}|$ \n",
    "\n",
    "where:  \n",
    "- $|V_{+}>$ : eigenvector for the positive eigenvalue  \n",
    "- $|V_{-}>$ : eigenvector for the negative eigenvalue  \n",
    "  \n",
    "As calculated for the case of five particles in the supplemental material: [Helstrom with 5 particles](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-HelstromWith5Particles.ipynb), the maximum probability of success for the Helstrom measurement in a sequence of 5 particles is given by:  \n",
    "  \n",
    "&emsp; $P_{Helstrom} = \\frac{1}{5}(-2k^4+5k^3+2k^2)$  \n",
    "  \n",
    "with $k=\\frac{1}{2}(1+\\sqrt{1-c^2})$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3. Bayesian Helstrom measurement <a class=\"anchor\" id=\"bayesian-helstrom-measurement\"></a> {-}\n",
    "\n",
    "The Bayesian measurement uses the results obtained after each particle measurement to adjust the measurement device using Bayesian inference. In the bayesian strategy we start with a uniform prior for every hypothesis, i.e. $P(H_i)=\\frac{1}{n}$ for $n$ particles, and after each measurement these priors are updated, using the Bayes' theorem, based on the previous results:  \n",
    "  \n",
    "&emsp; $P(H_i|\\bar{x}) = \\frac{ P(\\bar{x}|H_i)P(H_i)}{P(\\bar{x})}$  \n",
    "  \n",
    "where:  \n",
    "&emsp; $\\bar{x}$ represents the result obtained after the measurement  \n",
    "&emsp; $P(\\bar{x}|H_i)$ represents the probability of obtaining the result $\\bar{x}$ given the hypothesis $H_i$  \n",
    "&emsp; $P(H_i)$ is the probability of hypothesis $i$  \n",
    "&emsp; $P(\\bar{x})$ probability of obtaining outcome $\\bar{x}$  \n",
    "&emsp; $P(H_i|\\bar{x})$ is the new updated probability for the hypothesis $i$    \n",
    "  \n",
    "Notice this update of priors knowledge using Bayes theorem can be implemented with any measurement. In our case it has been implemented with Helstrom.\n",
    "\n",
    "Performing the Bayesian Helstrom is an iterative process we can summarize as follows:  \n",
    "\n",
    "**1. Build the matrix $\\Gamma = f(P_{\\Psi}, P_{\\Phi}, \\theta)$.**  \n",
    "\n",
    "For a given value of $\\theta$, the matrix depends only on $P_{\\Psi}$ and $P_{\\Phi}$, and those probabilities can be calculated taking into consideration the *contribution* of each hypothesis at the correspondig step of the experiment. For example, when measuring the Particle #4 (see table at section 1.1) the hypothesis contributing to $P_{\\Phi}$ are $P(H_1)$, $P(H_2)$, $P(H_3)$ and $P(H_4)$, and only $P(H_5)$ contributes to $P_{\\Psi}$  \n",
    "  \n",
    "As it is shown in <a href=\"#ref-1\">\\[1\\]</a> we can compute $P_{\\Psi}$ and $P_{\\Phi}$ using a *greedy strategy*, i.e. strategies that maximize the success probability at every step. <br>  \n",
    "  \n",
    "**2. Diagonalize $\\Gamma$ and build the Helstrom measurement.**  \n",
    "\n",
    "Diagonalize the resulting matrix and obtain its positve and negative eigenvalues ($V_{+}$ and $V_{-}$). Use those eigenvalues to build the new Helstrom measurement  \n",
    "  \n",
    "&emsp; {$E_{\\Psi} = |V_{+}><V_{+}|$, $E_{\\Phi} = |V_{-}><V_{-}|$}  \n",
    "  \n",
    "**3. Perform the measurement and get result.**  \n",
    "\n",
    "Using the newly build Helstrom measurement device, perform the measurement over the incoming particle, and get the result $\\Psi$ or $\\Phi$. \n",
    "  \n",
    "**4. Update the priors of each hypothesis using Bayes theorem.**  \n",
    "\n",
    "At any given step $s$, we can update the priors of each hypothesis using Bayesian inference. For example, suppose we have got measurement outcome $\\Psi$. To calculate the probability of the first hypothesis we would use Bayesian inference as follows:  \n",
    "  \n",
    "&emsp; $P(H_1|\\Psi)^{(s+1)} = \\frac{ P(\\Psi|H_1)P(H_1)^{(s)}}{P(\\Psi)^{(s)}}$  \n",
    "  \n",
    "The other four hypothesis are calculated similarly.  \n",
    "  \n",
    "**5. Evaluate the measurement.**  \n",
    "\n",
    "If the measurement is equal to $\\Psi$ stop and check the hypothesis with the highest probability, otherwise repeat the process again with the updated priors.  \n",
    "  \n",
    "Unlike for the Fully bias or Helstrom measurement, for the Bayesian Helstrom measurement we do not a have an analytical formula to compute the success probability for the 5 particle case. In this measurement what we did was to compute numerically the success probability. More details regarding this iterative process can be found on the supplemental material: [Bayesian with Helstrom](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-BayesianWithHelstrom.ipynb).  \n",
    "  \n",
    "Additionally, for the [Results comparison](#results-comparison) section we have calculated the Bayesian Helstrom experimentally using the Global and Local approach, i.e. without stopping and stopping when the state $\\Phi$ is measured, respectively. As in the Fully Bias case, the probability of success of the Bayesian Helstrom increases with the Local approach."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.4. Results comparison <a class=\"anchor\" id=\"results-comparison\"></a> {-}\n",
    "\n",
    "To compare the performance of the studied online strategies (Fully bias, Helstrom and Bayesian Helstrom) we have computed for all of them the theoretical and experimental values, and ploted the results in the graphic below. The result has been calculated using 5 particles and 20 different values of 'c' (overlap between states). Additionally, in the case of the experimental results computed in the IBM Q Experience, every experiment has been executed 1000 times. <br>\n",
    "\n",
    "Notice in the graphic below the theoretical results are represented by a continuous line, and the experimental results by a dotted line. As a reference of the optimal measurement we have also included the Square Root Measurement. <br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Experimental results for 20 values of 'c' and 1000 shots](images/graph-20-1000.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Theoretical results**\n",
    "\n",
    "To plot our theoretical results we have use the following probabilities of success for each measurement:  \n",
    "  \n",
    "* *Fully bias (Basic Local):*  \n",
    "&emsp; $P_{BL} = 1 - c^2 + \\frac{c^2}{5}$  \n",
    "  \n",
    "* *Helstrom:*  \n",
    "&emsp; $P_{Helstrom} = \\frac{1}{5}(-2k^4+5k^3+2k^2)$  \n",
    "&emsp; with $k=\\frac{1}{2}(1+\\sqrt{1-c^2})$  \n",
    "&emsp; As calculated for the case of five particles in the supplemental material: [Helstrom with 5 particles](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-HelstromWith5Particles.ipynb)  \n",
    "  \n",
    "* *Bayesian Helstrom:*  \n",
    "&emsp; The Bayesian Helstrom results have been calculated numerically following the step by step described in the supplemental material: [Bayesian with Helstrom](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-BayesianWithHelstrom.ipynb)  \n",
    "  \n",
    "* *Square Root Measurement*  \n",
    "&emsp; $P_{SQR} = \\frac{Tr\\sqrt(G)}{n}$  \n",
    "&emsp; with $G$ being the Gram matrix defined as $G_{xy} = <\\tilde{H_x}|\\tilde{H_y}>$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Experimental results**\n",
    "\n",
    "The experimental results have been obtained by developing three experiments using Qiskit, and executing them 1000 times in the simulator of the IBM Q Experience. Following we sketch the program developed which can be found in the Git repository of the project.  \n",
    "  \n",
    "When running the experiments the quantum circuits in general have two steps (see figure below):\n",
    "- A first [U3 gate](https://qiskit.org/documentation/stubs/qiskit.circuit.library.U3Gate.html) is performed to convert the qubit $|0>$ in $|\\Psi>$ or $|\\Phi>$  \n",
    "- A second [U3 gate](https://qiskit.org/documentation/stubs/qiskit.circuit.library.U3Gate.html) is performed to measure with the corresponding measurement operators.\n",
    "  \n",
    "The second unitary matrix is modified for each experiment as follows:  \n",
    "- Fully Bias measurement: the second unitary matrix can be safely replaced by the Identify matrix.  \n",
    "- Helstrom measurement: the second unitary matrix is modified according to the probabilities determined by the Hypothesis at each step in the secuence.  \n",
    "- Bayesian Helstrom: the second unitary is modified according to the probabilities determined using Bayesian inference.  \n",
    "  \n",
    "In the program developed each quantum system is measured sequentially. Both for Helstrom and Bayesian Helstrom after each measurement, classical calculations are done to obtain the new unitary matrix. And in the case of Bayesian Helstrom, using the results of the previous measurement."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Helstrom circuit](images/circuit.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Local vs. Global approach**\n",
    "\n",
    "As it has been explained for the case of the Fully bias measurement, there are two possible approaches when dealing with the detection of the $\\Psi$ state. A 'local' approach, i.e. stop the measurement of the sequence as soon as the $\\Psi$ state is measured, or a 'global' approach, i.e. measure the whole sequence up until the fifth particle. <br>\n",
    "\n",
    "Unlike the Fully bias measurement, that provides certainty on the detection of the $\\Psi$ state, the Bayesian Helstrom measurement can produce an erroneus measurement, i.e. there is a non-zero probability of measuring $\\Psi$ when the state is $\\Phi$. To better understand the implications of both approaches on the Bayesian Helstrom measurement, we ran the experiment 1000 times each. The results of the experiment can be found in the plot below. <br>\n",
    "\n",
    "As it can be seen in the graphic, the similarity between the results justify the adoption of the local strategy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Bayesian local vs. Bayesian global](images/local-global.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Future work <a class=\"anchor\" id=\"future-work\"></a> {-}\n",
    " \n",
    "As explained in [[2]](#reference-2) both Helstrom and Bayesian Helstrom measurements have non-zero probability of misidentifying a state, since the objective of both measurements is to minimize the rate of missing the change point, hence their type is called ambiguous measurements. <br>\n",
    "\n",
    "As a future work we could explore another category of measurements called *unambiguous measurements*. This type of measurements are interesting in certain situations where the goal is still minimizing the rate of missing the change point but **under the constraint** of not misidentifying a state. <br>\n",
    "\n",
    "For example, given the hypothesis $H_4$ (i.e. $|\\Psi> |\\Psi> |\\Psi> |\\Phi> |\\Phi>$), an *ambiguous measurement* could produce the measurement outcome $|\\Psi> |\\Psi> |\\Phi> |\\Phi> |\\Phi>$, efectively missidentifying the state at step 3. Whether an *unambiguous measurement* could produce an inconclusive measurement at the same step (i.e.  $|\\Psi> |\\Psi> |???> |\\Phi> |\\Phi>$). Here we denote as $|???>$ as the inconclusive measurement. <br>\n",
    "\n",
    "In an unambiguous measurement each quantum system in the sequence is measured using a Positive Operator Valued Measure (POVM) consisting of three elements, two to distinguish unambiguously the two states and one to signal an inconclusive outcome. <br>\n",
    "  \n",
    "In this measurement strategy each state in the sequence is measured using the following POVM:  \n",
    "   \n",
    "&emsp; $E_{\\Psi} = \\mu_1|\\Psi^{\\bot}><\\Psi^{\\bot}|$  \n",
    "&emsp; $E_{\\Phi} = \\mu_0|\\Phi^{\\bot}><\\Phi^{\\bot}|$  \n",
    "&emsp; $E_{\\epsilon} = 1 - E_{\\Psi} - E_{\\Phi}$  \n",
    "  \n",
    "The possible measurement outcomes with this POVM are:  \n",
    "- If we get $E_{\\Psi}$ we are 100% sure the state is $\\Psi$ and not $\\Phi$  \n",
    "- If we get $E_{\\Phi}$ we are 100% sure the state is $\\Phi$ and not $\\Psi$  \n",
    "- If we get $E_{\\epsilon}$ we have an inconclusive result.  \n",
    "  \n",
    "As computed in the supplemental material on section: [Unambiguous binary state discrimination](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-povm.ipynb), the minimum probability of obtaining an inconclusive result is given by  \n",
    "  \n",
    "&emsp; $P(E_{\\epsilon})^{min} = 2\\sqrt{P_{\\Psi}P_{\\Phi}}<\\Psi|\\Phi>$  \n",
    "  \n",
    "where:  \n",
    "&emsp; $P_{\\Psi}$: probability of state $\\Psi$.  \n",
    "&emsp; $P_{\\Phi}$: probability of state $\\Phi$.  \n",
    "  \n",
    "To build such POVM we can rely on Naimark's theorem to obtain it from a projective measurement acting on a larger space, i.e. by *expanding* the Hilbert space of $\\Psi$ and $\\Phi$ using an ancilla system. Naimark's theorem  give us a way to physically realize POVM measurements using qutrits.  \n",
    "  \n",
    "Since qutrits are not an avaible resource on the IBM Q Experience, we would need to use Peres[[5]](#reference-5) simplification to construct our POVM using qubits."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Conclusion <a class=\"anchor\" id=\"conclusion\"></a> {-}\n",
    "\n",
    "As we have seen in the report, the online strategies discussed in this document perform worse than the optimal Square Root Measurement. Nevertheless, they provide the benefit of an online answer without the need to wait for the whole sequence of particles and the use of a quantum memory that may be infeasible.  \n",
    "  \n",
    "For the report we have calculated the theoretical probabilities for three online strategies (Fully bias, Helstrom and Bayesian Helstrom) and compared their performance against the optimal measurement. This strategies are easier to implement than the Square Root Measurement, and allow the detection of the change in the stream of quantum systems as soon as it happens. Throught the project we have also implemented the online strategies using the IBM Q Experience platform, and experimentally calculated the probabilities, obtaining similar results to those predicted theoretically."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. References <a class=\"anchor\" id=\"references\"></a> {-}\n",
    "\n",
    "\\[1\\] [The quantum change point](https://arxiv.org/abs/1605.01916) : Gael Sentís, Emilio Bagan, John Calsamiglia, Giulio Chiribella, and Ramón Muñoz-Tapia. Phys. Rev. Lett. 117, 150502 – Published 7 October 2016 <a class=\"anchor\" id=\"reference-1\"></a>\n",
    "\n",
    "\\[2\\] [Exact identification of a Quantum Change Point](https://arxiv.org/abs/1707.07769) : Gael Sentís, John Calsamiglia, Ramon Munoz-Tapia. Phys. Rev. Lett. 119, 140506 – Published 6 October 2017 <a class=\"anchor\" id=\"reference-2\"></a>\n",
    "\n",
    "\\[3\\] [Experimentally detecting a quantum change point via Bayesian inference](https://arxiv.org/abs/1801.07508) : Shang Yu, Chang-Jiang Huang, Jian-Shun Tang, Zhih-Ahn Jia, Yi-Tao Wang, Zhi-Jin Ke, Wei Liu, Xiao Liu, Zong-Quan Zhou, Ze-Di Cheng, Jin-Shi Xu, Yu-Chun Wu, Yuan-Yuan Zhao, Guo-Yong Xiang, Chuan-Feng Li, Guang-Can Guo, Gael Sentís, Ramon Muñoz-Tapia. Phys. Rev. A98, 040301(R) – Published 15 October 2018 <a class=\"anchor\" id=\"reference-3\"></a>\n",
    "\n",
    "\\[4\\] [Online strategies for exactly identifying a quantum change point](https://arxiv.org/abs/1802.00280) : Gael Sentís, Esteban Martínez-Vargas, Ramon Muñoz-Tapia. Phys. Rev. A98, 052305 – Published 5 November 2018 <a class=\"anchor\" id=\"reference-4\"></a>  \n",
    "  \n",
    "\\[5\\] [How to differentiate betweeen non-orthogonal states](https://ur.booksc.org/ireader/2993780) : Asher Peres. Phys. Letter. Volume 128, number 1.– Published 21 March 1988 <a class=\"anchor\" id=\"reference-5\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. Supplemental material <a class=\"anchor\" id=\"supplemental\"></a> {-}\n",
    "- [Probability of success for one state](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-ProbHelstrom1state.ipynb): probability of success for one state using Helstrom\n",
    "- [Helstrom proof](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-ProofHelstrom.ipynb): demonstration of Helstrom as the strategy that maximizes the probability of success\n",
    "- [Helstrom with 5 particles](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-HelstromWith5Particles.ipynb): analytical solution of Helstrom for 5 particles\n",
    "- [Bayesian with Helstrom](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-BayesianWithHelstrom.ipynb): step by step how to use Helstrom updating the priors using Bayesian\n",
    "- [Unambiguous binary state discrimination](https://gitlab.com/quantum.engineering/quantum-change-point/-/blob/master/Annex-povm.ipynb): proof the minimum error probability"
   ]
  }
 ],
 "metadata": {
  "abstract": "The change point problem is a well known problem in statistics aiming at detecting a change in a sequence of random variables. In our project we studied the quantum equivalent called 'quantum change point' mainly based on the ideas described in Gael et al.",
  "authors": [
   {
    "name": "Isidoro Legido"
   },
   {
    "name": "Victor Gaspar"
   }
  ],
  "description": "Final report for the postgraduate course on Quantum Engineering at the Universitat Politènica de Catalunya (UPC) school. Written by Isidoro Legido and Victor Gaspar, and supervised by Michalis Skotiniotis.",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "keywords": "Quantum, measurement, fully bias, Helstrom, POVM",
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.1"
  },
  "subtitle": "Detecting sudden changes in the quantum domain",
  "title": "The Quantum Change Point"
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
